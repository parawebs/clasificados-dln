$('#nav').affix({
    offset: {
        top: function() {
            return ($('header').height())
        }
    }
});
Parse.initialize("bAMJZdvRxC8RccVElUnr0E16Zxxsn0mCj6WwWKFq", "cI04mDb8o67hrUgMEYbMCma4Bmt0hMU4cIa845pF");

var current;
var sections = [{
    "cod_sec": "0",
    "descripcion": "Index"
}, {
    "cod_sec": "1",
    "descripcion": "Autos Compra-Venta"
}, {
    "cod_sec": "10",
    "descripcion": "Posadas Turisticas"
}, {
    "cod_sec": "11",
    "descripcion": "Fincas y Granjas"
}, {
    "cod_sec": "12",
    "descripcion": "Locales y Oficinas Compra - Venta"
}, {
    "cod_sec": "13",
    "descripcion": "Locales y Oficinas en Alquiler"
}, {
    "cod_sec": "14",
    "descripcion": "Negocios Compra - Venta"
}, {
    "cod_sec": "15",
    "descripcion": "Objetos Compra - Venta"
}, {
    "cod_sec": "16",
    "descripcion": "Casas de Empe\u00f1o"
}, {
    "cod_sec": "17",
    "descripcion": "Empleos Ofertas"
}, {
    "cod_sec": "17a",
    "descripcion": "Vendedores"
}, {
    "cod_sec": "17b",
    "descripcion": "Servicios Domesticos"
}, {
    "cod_sec": "18",
    "descripcion": "Empleos Demanda"
}, {
    "cod_sec": "19",
    "descripcion": "Profesiones Varias"
}, {
    "cod_sec": "1a",
    "descripcion": "Camionetas"
}, {
    "cod_sec": "1b",
    "descripcion": "Jeeps"
}, {
    "cod_sec": "1c",
    "descripcion": "Camiones y Autobuses"
}, {
    "cod_sec": "2",
    "descripcion": "Motos"
}, {
    "cod_sec": "20",
    "descripcion": "Institutos y Academias"
}, {
    "cod_sec": "21",
    "descripcion": "Computaci\u00f3n"
}, {
    "cod_sec": "22",
    "descripcion": "Servicios de Mantenimiento"
}, {
    "cod_sec": "23",
    "descripcion": "Plomerias"
}, {
    "cod_sec": "24",
    "descripcion": "Maquinarias"
}, {
    "cod_sec": "25",
    "descripcion": "Animales"
}, {
    "cod_sec": "26",
    "descripcion": "Libros y Revistas"
}, {
    "cod_sec": "27",
    "descripcion": "Materiales de Construcci\u00f3n"
}, {
    "cod_sec": "3",
    "descripcion": "Casas Compra - Venta"
}, {
    "cod_sec": "4",
    "descripcion": "Casas en Alquiler"
}, {
    "cod_sec": "5",
    "descripcion": "Casas en el Interior"
}, {
    "cod_sec": "6",
    "descripcion": "Apartamentos Compra - Venta"
}, {
    "cod_sec": "7",
    "descripcion": "Apartamentos en Alquiler"
}, {
    "cod_sec": "8",
    "descripcion": "Habitaciones"
}, {
    "cod_sec": "9",
    "descripcion": "Terrenos Compra - Venta"
}];
var current = {
    cat: 0,
    id: 0
}

function paginate(total, visible) {

    try {
        $('#pagination2').twbsPagination('destroy');
    } catch (e) {
        console.log(e);
    }
    $('#pagination2').twbsPagination({
        totalPages: total,
        visiblePages: visible,
        onPageClick: function(event, page) {
            skip = Math.ceil(page * 5);
            render(current.cat, current.id, skip)
        }
    });

}

function getCount(cat, id) {

    var query = new Parse.Query("Clasificados");
    query.equalTo("seccion", id.toLowerCase());
    query.count().then(function(results) {
        total = Math.ceil(results / 5);
        paginate(total, 5);
    });
}

function render(cat, id, skip) {

    $(".main").fadeOut('slow/400/fast', function() {
        $(".detailed").show();
    });
    id = id || "0";
    skip = skip || 0;
    var sectionText = _.filter(sections, {
        "cod_sec": id.toString()
    });
    var query = new Parse.Query("Clasificados");
    query.equalTo("seccion", id.toLowerCase());
    query.limit(5)
    query.skip(skip)
    query.find().then(function(results) {
        clasificados = []
        if (results.length > 0) {
            for (var i = 0; i < results.length; i++) {
                var object = results[i];
                clasificados.push({
                    seccion: sectionText[0]['descripcion'],
                    factura: object.get('factura'),
                    abc: object.get('abc'),
                    aviso: object.get('aviso')
                });
            }
        }
        return clasificados

    }).then(function(clasificados) {
        riot.mount('clasificados', {
            items: clasificados
        })

    });
    document.body.scrollTop = document.documentElement.scrollTop = 0;

}
$(document).ready(function() {
    riot.route.exec(function(cat, id) {
        if (cat.length > 0) {
            render(cat, id);
            current = {
                cat: cat,
                id: id
            }
            getCount(cat, id);


        }
    });
    riot.route(function(cat, id) {
        render(cat, id);
        current = {
            cat: cat,
            id: id
        }
        getCount(cat, id);
    });

});


var endPoint = "https://api.parse.com/1/classes/Clasificados?"
$("#search2").on("click", function(event) {
    $('#loadimg').show();
    var terms = $("#searchTerm2").val().toLowerCase().trim().split(" ");
    var stopWords = ["a", "actualmente", "acuerdo", "adelante", "ademas", "además", "adrede", "afirmó", "agregó", "ahi", "ahora", "ahí", "al", "algo", "alguna", "algunas", "alguno", "algunos", "algún", "alli", "allí", "alrededor", "ambos", "ampleamos", "antano", "antaño", "ante", "anterior", "antes", "apenas", "aproximadamente", "aquel", "aquella", "aquellas", "aquello", "aquellos", "aqui", "aquél", "aquélla", "aquéllas", "aquéllos", "aquí", "arriba", "arribaabajo", "aseguró", "asi", "así", "atras", "aun", "aunque", "ayer", "añadió", "aún", "b", "bajo", "bastante", "bien", "breve", "buen", "buena", "buenas", "bueno", "buenos", "c", "cada", "casi", "cerca", "cierta", "ciertas", "cierto", "ciertos", "cinco", "claro", "comentó", "como", "con", "conmigo", "conocer", "conseguimos", "conseguir", "considera", "consideró", "consigo", "consigue", "consiguen", "consigues", "contigo", "contra", "cosas", "creo", "cual", "cuales", "cualquier", "cuando", "cuanta", "cuantas", "cuanto", "cuantos", "cuatro", "cuenta", "cuál", "cuáles", "cuándo", "cuánta", "cuántas", "cuánto", "cuántos", "cómo", "d", "da", "dado", "dan", "dar", "de", "debajo", "debe", "deben", "debido", "decir", "dejó", "del", "delante", "demasiado", "demás", "dentro", "deprisa", "desde", "despacio", "despues", "después", "detras", "detrás", "dia", "dias", "dice", "dicen", "dicho", "dieron", "diferente", "diferentes", "dijeron", "dijo", "dio", "donde", "dos", "durante", "día", "días", "dónde", "e", "ejemplo", "el", "ella", "ellas", "ello", "ellos", "embargo", "empleais", "emplean", "emplear", "empleas", "empleo", "en", "encima", "encuentra", "enfrente", "enseguida", "entonces", "entre", "era", "eramos", "eran", "eras", "eres", "es", "esa", "esas", "ese", "eso", "esos", "esta", "estaba", "estaban", "estado", "estados", "estais", "estamos", "estan", "estar", "estará", "estas", "este", "esto", "estos", "estoy", "estuvo", "está", "están", "ex", "excepto", "existe", "existen", "explicó", "expresó", "f", "fin", "final", "fue", "fuera", "fueron", "fui", "fuimos", "g", "general", "gran", "grandes", "gueno", "h", "ha", "haber", "habia", "habla", "hablan", "habrá", "había", "habían", "hace", "haceis", "hacemos", "hacen", "hacer", "hacerlo", "haces", "hacia", "haciendo", "hago", "han", "hasta", "hay", "haya", "he", "hecho", "hemos", "hicieron", "hizo", "horas", "hoy", "hubo", "i", "igual", "incluso", "indicó", "informo", "informó", "intenta", "intentais", "intentamos", "intentan", "intentar", "intentas", "intento", "ir", "j", "junto", "k", "l", "la", "lado", "largo", "las", "le", "lejos", "les", "llegó", "lleva", "llevar", "lo", "los", "luego", "lugar", "m", "mal", "manera", "manifestó", "mas", "mayor", "me", "mediante", "medio", "mejor", "mencionó", "menos", "menudo", "mi", "mia", "mias", "mientras", "mio", "mios", "mis", "misma", "mismas", "mismo", "mismos", "modo", "momento", "mucha", "muchas", "mucho", "muchos", "muy", "más", "mí", "mía", "mías", "mío", "míos", "n", "nada", "nadie", "ni", "ninguna", "ningunas", "ninguno", "ningunos", "ningún", "no", "nos", "nosotras", "nosotros", "nuestra", "nuestras", "nuestro", "nuestros", "nueva", "nuevas", "nuevo", "nuevos", "nunca", "o", "ocho", "os", "otra", "otras", "otro", "otros", "p", "pais", "para", "parece", "parte", "partir", "pasada", "pasado", "paìs", "peor", "pero", "pesar", "poca", "pocas", "poco", "pocos", "podeis", "podemos", "poder", "podria", "podriais", "podriamos", "podrian", "podrias", "podrá", "podrán", "podría", "podrían", "poner", "por", "porque", "posible", "primer", "primera", "primero", "primeros", "principalmente", "pronto", "propia", "propias", "propio", "propios", "proximo", "próximo", "próximos", "pudo", "pueda", "puede", "pueden", "puedo", "pues", "q", "qeu", "que", "quedó", "queremos", "quien", "quienes", "quiere", "quiza", "quizas", "quizá", "quizás", "quién", "quiénes", "qué", "r", "raras", "realizado", "realizar", "realizó", "repente", "respecto", "s", "sabe", "sabeis", "sabemos", "saben", "saber", "sabes", "salvo", "se", "sea", "sean", "segun", "segunda", "segundo", "según", "seis", "ser", "sera", "será", "serán", "sería", "señaló", "si", "sido", "siempre", "siendo", "siete", "sigue", "siguiente", "sin", "sino", "sobre", "sois", "sola", "solamente", "solas", "solo", "solos", "somos", "son", "soy", "soyos", "su", "supuesto", "sus", "suya", "suyas", "suyo", "sé", "sí", "sólo", "t", "tal", "tambien", "también", "tampoco", "tan", "tanto", "tarde", "te", "temprano", "tendrá", "tendrán", "teneis", "tenemos", "tener", "tenga", "tengo", "tenido", "tenía", "tercera", "ti", "tiempo", "tiene", "tienen", "toda", "todas", "todavia", "todavía", "todo", "todos", "total", "trabaja", "trabajais", "trabajamos", "trabajan", "trabajar", "trabajas", "trabajo", "tras", "trata", "través", "tres", "tu", "tus", "tuvo", "tuya", "tuyas", "tuyo", "tuyos", "tú", "u", "ultimo", "un", "una", "unas", "uno", "unos", "usa", "usais", "usamos", "usan", "usar", "usas", "uso", "usted", "ustedes", "v", "va", "vais", "valor", "vamos", "van", "varias", "varios", "vaya", "veces", "ver", "verdad", "verdadera", "verdadero", "vez", "vosotras", "vosotros", "voy", "vuestra", "vuestras", "vuestro", "vuestros", "w", "x", "y", "ya", "yo", "z", "él", "ésa", "ésas", "ése", "ésos", "ésta", "éstas", "éste", "éstos", "última", "últimas", "último", "últimos"];
    words = _.filter(terms, function(w) {
        return (w.length > 1) && !(_.contains(stopWords, w));
    });
    var search = encodeURIComponent('where={"terms":{"$all":' + JSON.stringify(words) + '}}');
    $.ajax({
        beforeSend: function(request) {
            request.setRequestHeader('X-Parse-Application-Id', 'bAMJZdvRxC8RccVElUnr0E16Zxxsn0mCj6WwWKFq');
            request.setRequestHeader('X-Parse-REST-API-Key', 'nZkC5ox2IBbk6U2exwtFimskPxlQdrNxyi15eLgD');
            request.setRequestHeader('Content-Type', 'application/json');
        },
        dataType: "json",
        url: endPoint + search,
        success: function(data) {
            var clasificados = [];
            for (var i = 0; i < data.results.length; i++) {
                var object = data.results[i];
                var sectionText = _.filter(sections, {
                    "cod_sec": object.seccion
                });

                clasificados.push({
                    seccion: sectionText[0]['descripcion'],
                    factura: object.factura,
                    abc: object.abc,
                    aviso: object.aviso
                });
            }

            riot.mount('clasificados', {
                items: clasificados
            })
        }
    });
});
